import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Area;
import java.util.HashSet;

import javax.swing.*;


public class Game extends JPanel implements KeyListener, ActionListener {
	
	private int height, width;
	private Timer t = new Timer(5, this);
	private boolean first;
	private boolean newFeed;

	private HashSet<String> keys = new HashSet<String>();
	
	private double speed = 1;
	private int inset = 40;
	private int way = 0;
	
	private double snakeX, snakeY, snakeSize = 15;
	private double feedX, feedY, feedSize = 5;
	
	private int scorePlayer;
	

	public Game() {
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		first = true;
		newFeed = true;
		t.setInitialDelay(100);
		t.start();
	}
	
	//Fonction d'�criture graphique
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		height = getHeight();
		width = getWidth();

		if (first) {
			snakeX = width / 2;
			snakeY = height / 2;
			first = false;
		}
		
		if (newFeed) {
			feedX = (int)(Math.random() * (width - feedSize));
			feedY = (int)(Math.random() * (height - feedSize) + inset);
			newFeed = false;
		}
		
		Rectangle2D snake = new Rectangle.Double(snakeX, snakeY, snakeSize, snakeSize);
		g2d.fill(snake);
		
		Rectangle2D feed = new Rectangle.Double(feedX, feedY, feedSize, feedSize);
		g2d.fill(feed);
		
		Rectangle2D barTop = new Rectangle(0, inset - 5, width, 5);
		g2d.fill(barTop);
		
		String scoreB = "Player : " + new Integer(scorePlayer).toString();
		g2d.drawString(scoreB, 10, 20);
		
		if (chkColl(snake, feed) == 1) {
			scorePlayer++;
			newFeed = true;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (way == 0) {
			snakeY = snakeY - speed;
		}
		else if (way == 1) {
			snakeX = snakeX + speed;
			}
		else if (way == 2) {
			snakeY = snakeY + speed;
		}
		else if (way == 3) {
			snakeX = snakeX - speed;
		}
		
		if (snakeY == inset || snakeY == (height - snakeSize)) {
			first = true;
			scorePlayer = 0;
		}
		if (snakeX == 0 || snakeX == (width - snakeSize)) {
			first = true;
			scorePlayer = 0;
		}
		
		
		if (keys.size() == 1) {
			if (keys.contains("UP")) {
				way = 0;
			}
			else if (keys.contains("RIGHT")) {
				way = 1;
			}
			else if (keys.contains("DOWN")) {
				way = 2;
			}
			else if (keys.contains("LEFT")) {
				way = 3;
			}
		}

		repaint();
	}
	
	public int chkColl(Rectangle2D s, Rectangle2D f) {
		Area areaS = new Area(s);
		if (areaS.intersects(f) == true)
			return 1;
		else
			return 0;
	}

	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		switch (code) {
		case KeyEvent.VK_LEFT:
			keys.add("LEFT");
			break;
		case KeyEvent.VK_RIGHT:
			keys.add("RIGHT");
			break;
		case KeyEvent.VK_UP:
			keys.add("UP");
			break;
		case KeyEvent.VK_DOWN:
			keys.add("DOWN");
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int code = e.getKeyCode();
		switch (code) {
		case KeyEvent.VK_LEFT:
			keys.remove("LEFT");
			break;
		case KeyEvent.VK_RIGHT:
			keys.remove("RIGHT");
			break;
		case KeyEvent.VK_UP:
			keys.remove("UP");
			break;
		case KeyEvent.VK_DOWN:
			keys.remove("DOWN");
			break;
		}
	}
}
